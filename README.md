The primary purpose of the SCOOP template engine (`ste`) is to facilitate the preparation of manuscripts in [LaTeX](https://www.latex-project.org/) for publication in scientific journals.
It allows the user to concentrate on the content, rather than the layout.
The layout, which depends on the journal, will be automatically generated.
An effort is made to achieve compatibility of a range of standard LaTeX packages with each supported journal.
In addition, a consistent set of theorem-like environments is provided across journals.

As of version 1.5.0, the SCOOP template engine also supports the creation of LaTeX letters.

## Installation
Regular users are advised to install [from PyPI](https://pypi.org/project/scoop-template-engine/):
```python
pip3 install scoop-template-engine
ste init
```

## Getting Started
Please refer to the documentation [from PyPI](https://pypi.org/project/scoop-template-engine/).
