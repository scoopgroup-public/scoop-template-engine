# This script initializes the LaTeX author resources provided for
# Mathematical Modelling and Numerical Analysis (ESAIM: M2AN).
# https://www.esaim-m2an.org/author-information/latex2e-macro-package

import sys
from ste.utilities import utilities
from subprocess import run, STDOUT

if __name__ == '__main__':
    try:
        # Remove the initialization time and version stamp.
        utilities.remove_time_version_stamp()

        # Get and unpack the LaTeX author resources from the publisher.
        utilities.get_archive('https://www.esaim-m2an.org/doc_journal/instructions/macro/m2an/macro-latex-m2an.zip', junk = 1)

        # Get the separate style manual as well.
        utilities.get_file('https://www.esaim-m2an.org/images/stories/instructions/m2an_instructions.pdf')

        # Compile the template file.
        run(['latexmk', '-pdf', '-norc', '-interaction=nonstopmode',  'template.tex'], stderr = STDOUT)
        run(['latexmk', '-c'  , '-norc', '-interaction=nonstopmode',  'template.tex'], stderr = STDOUT)

        # Write the initialization time and version stamp.
        utilities.write_time_version_stamp()

    except:
        sys.exit(1)
