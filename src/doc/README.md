This directory contains the source code for the documenation to the SCOOP template engine (`ste`) and to its companion package, the SCOOP prepare BibTeX file (`\spbf`).
The [LICENSE](LICENSE) file applies to [markup.py](markup.py), which is modified from its original version [Pygments](https://pygments.org/) and distributed under the same BSD 2-Clause "simplified" license.
